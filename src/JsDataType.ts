export default class JsDataType {
    static string: 'string' = 'string'
    static bigint: 'bigint' = 'bigint'
}
