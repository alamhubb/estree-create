import type {Literal} from "estree";
import EstreeLiteralCreate from "./EstreeLiteralCreate.ts";

export default class EstreeCreate {
    static literal(value: string | boolean | number | RegExp | bigint | null | undefined): Literal {
        return EstreeLiteralCreate.literal(value)
    }
}
